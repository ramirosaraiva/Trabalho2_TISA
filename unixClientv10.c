
/*  
    Compiled through command 'gcc -pthread -o client unixClient.c -lrt' 
    or './compileUCS'.
	To run: ./client
*/

#include "ucs.h" 

// Threads execution period (in seconds). 
#define DATA_REQUEST_TS		5.0f 	// data request: 5.0 s.	

// Global variables.
bool PrintOnScreen;
bool QuitProgram;
bool RequestDataOn;
bool SendACKNACKOn;
bool ChecksumErrorOn;
bool ShowGeneralMsgOn;

float TempMeas = 0.0f; 	// Temperature measurement.
struct BufferMessageUCS BufferPendingMsg; // Buffer of pending messages.
pthread_mutex_t ReadWriteIO = PTHREAD_MUTEX_INITIALIZER;

void RequestData();
void ReceiveData();

// ------------------------------------

int main (int argc, char *argv[]) 
{
	// Main function.		
	
	char comando;	
	bool quitP, printS, reqData, sendAckNack, checksumError, ShowGeneralMsg;
	pthread_t thRequestData, thReceiveData;	
			
	printf("\n--- CLIENT ---\n");					
	
	// Initiate variables.
	BuffMsgInit(&ReadWriteIO, &BufferPendingMsg);
	quitP = false;	
	WriteGlobalVar(&ReadWriteIO, &QuitProgram, &quitP, DATA_BOOL);	
	printS = true;
	WriteGlobalVar(&ReadWriteIO, &PrintOnScreen, &printS, DATA_BOOL);
	reqData = true;
	WriteGlobalVar(&ReadWriteIO, &RequestDataOn, &reqData, DATA_BOOL);
	sendAckNack = true;
	WriteGlobalVar(&ReadWriteIO, &SendACKNACKOn, &sendAckNack, DATA_BOOL);
	checksumError = false;
	WriteGlobalVar(&ReadWriteIO, &ChecksumErrorOn, &checksumError, DATA_BOOL);
	ShowGeneralMsg = false;
	WriteGlobalVar(&ReadWriteIO, &ShowGeneralMsgOn, &ShowGeneralMsg, DATA_BOOL);
	

	// Create threads of execution.	
	pthread_create(&thRequestData, NULL, 
			(void*) RequestData, NULL); // Request data to server.	
	pthread_create(&thReceiveData, NULL, 
			(void*) ReceiveData, NULL); // Receive data from server.			
			
	while(!quitP)
	{
		// Wait for keyboard input command.		
		scanf("%c", &comando);		
		
		switch(comando)
		{	
			case 10: // <Enter> key, load Menu.
				// Stop printing process data on screen.
				printS = false;
				WriteGlobalVar(&ReadWriteIO, &PrintOnScreen, 
								&printS, DATA_BOOL);
				
				printf("Type '1'+<Enter> to start/stop client requests.\n");
				printf("Type '2'+<Enter> to start/stop sending ACK/NACK.\n");
				printf("Type '3'+<Enter> to start/stop sending wrong checksum.\n");
				printf("Type '4'+<Enter> to start/stop showing general messages.\n");
				printf("Type 'q'+<Enter> to quit program.\n");
				printf("Press <Enter> to leave menu.\n");	
				
				while(!printS)
				{
					// Wait for keyboard input command.		
					scanf("%c", &comando);
					
					switch(comando)
					{
						case '1':
							// Start/stop client requests.
							ReadGlobalVar(&ReadWriteIO, &RequestDataOn, 
											&reqData, DATA_BOOL);
							if (reqData == true)
							{
								// Turn off data requests.
								reqData = false;
								printf("Data requests turned off.\n");								
							}
							else
							{
								// Turn on data requests.
								reqData = true;								
								printf("Data requests turned on.\n");	
							}
							WriteGlobalVar(&ReadWriteIO, &RequestDataOn, 
												&reqData, DATA_BOOL);								
							break;
						
						case '2':
							// Start/stop sending ACK/NACK to server.
							ReadGlobalVar(&ReadWriteIO, &SendACKNACKOn, 
											&sendAckNack, DATA_BOOL);
							if (sendAckNack == true)
							{
								// Turn off ACK/NACK.
								sendAckNack = false;
								printf("ACK/NACK turned off.\n");								
							}
							else
							{
								// Turn on ACK/NACK.
								sendAckNack = true;								
								printf("ACK/NACK turned on.\n");	
							}
							WriteGlobalVar(&ReadWriteIO, &SendACKNACKOn, 
												&sendAckNack, DATA_BOOL);
							break;	
							
						case '3':
							// Start/stop sending wrong checksum to server.
							ReadGlobalVar(&ReadWriteIO, &ChecksumErrorOn, 
											&checksumError, DATA_BOOL);
							if (checksumError == true)
							{
								// Turn off checksum errors.
								checksumError = false;
								printf("Checksum errors turned off.\n");								
							}
							else
							{
								// Turn on checksum errors.
								checksumError = true;								
								printf("Checksum errors turned on.\n");	
							}
							WriteGlobalVar(&ReadWriteIO, &ChecksumErrorOn, 
												&checksumError, DATA_BOOL);
							break;	

						case '4':
							// Start/stop sending wrong checksum to server.
							ReadGlobalVar(&ReadWriteIO, &ShowGeneralMsgOn, 
											&ShowGeneralMsg, DATA_BOOL);
							if (ShowGeneralMsg == true)
							{
								// Turn off general messages.
								ShowGeneralMsg = false;
								printf("General Messages turned off.\n");								
							}
							else
							{
								// Turn on general messages.
								ShowGeneralMsg = true;								
								printf("General Messages turned on.\n");	
							}
							WriteGlobalVar(&ReadWriteIO, &ShowGeneralMsgOn, 
												&ShowGeneralMsg, DATA_BOOL);
							break;								


						case 'q':
							// Quit program.
							printS = true;
							WriteGlobalVar(&ReadWriteIO, &PrintOnScreen, 
											&printS, DATA_BOOL);
							// Terminate control loops.
							quitP = true;
							WriteGlobalVar(&ReadWriteIO, &QuitProgram, 
											&quitP, DATA_BOOL);							
							break;
							
						case 10: // <Enter> key, close Menu.
							// Restart printing process data on screen.
							printS = true;
							WriteGlobalVar(&ReadWriteIO, &PrintOnScreen, 
											&printS, DATA_BOOL);
							break;	
							
						default:
							printf("Unknown command.\n");
							break;
					}
						
				}				
				break;			
				
			default:
				break;
		}	
		
		ReadGlobalVar(&ReadWriteIO, &QuitProgram, &quitP, DATA_BOOL);
		
	} 

	// Does not finish main function until some threads have finished.
	// pthread_join(thRequestData, NULL);
	// pthread_join(thReceiveData, NULL);

	printf("BYE!\n\n");
	return 0; // end of program.
}	

// ------------------------------------

void RequestData()
{
	// Request data to server. 	
	int lastIdSearched =0;
	bool ShowGeneralMsg =0;										
	bool quitP = false;
	bool printS;
	bool reqData;
	char messageBuffer[BUF_SIZE];	
	long int thPeriodSec, thPeriodNsec;	
	int sock1;
	unsigned int linePrint = 0;
	unsigned int packID;
	float varRead;
	struct timespec thTime;	
	struct MessageUCS msgUCS,msgPendingUCS;
	
	// Convert thread execution period.
	thPeriodSec = (long int) DATA_REQUEST_TS;
	thPeriodNsec = (long int) ((DATA_REQUEST_TS-thPeriodSec)*NSEC_PER_SEC);
	
	// Start communication socket.		
	sock1 = GetCommunicationSocket(COMM_SERVER_IP, COMM_UDP_PORT_TO_SERVER, 
									COMM_TRANSMITTER);	
		
	// Get current time.
    clock_gettime(CLOCK_MONOTONIC ,&thTime);
    // Start after one second. 
    thTime.tv_sec++;    
    	
    packID = 0;    	
    
	while(!quitP)
	{
		// Wait until next execution period starts.
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &thTime, NULL);

		// get value of ShowGeneralMsg
			ReadGlobalVar(&ReadWriteIO, &ShowGeneralMsgOn, &ShowGeneralMsg, DATA_BOOL);
		// Start loop execution.
			
		// Check if data requests are turned on.
		ReadGlobalVar(&ReadWriteIO, &RequestDataOn, &reqData, DATA_BOOL);	
		if (reqData == true)
		{

						// check if there are messages pending in the buffer
						lastIdSearched =0;
						while(BuffMsgNext(&ReadWriteIO, 
								&BufferPendingMsg, &msgPendingUCS,lastIdSearched) == true)	
						{
							lastIdSearched = msgPendingUCS.packID;
							BuildMessageString(&msgPendingUCS, messageBuffer);
							// Send data to client.
							if (write(sock1, messageBuffer, strlen(messageBuffer)) 
								!= strlen(messageBuffer))
							{
								// Error sending response.
								if (ShowGeneralMsg)
									RefreshScreen(&ReadWriteIO, &PrintOnScreen, 
									&linePrint, "|##ERROR##| Request Again #", 
									&(msgPendingUCS.packID), DATA_INT);
							}
							else
							{
						    	//if (ShowGeneralMsg)
						    		RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
									"|CLIENT >>| Request Again #", &(msgPendingUCS.packID), DATA_INT);
							}	 

						}


			// Calculate package ID.
			//if buffer is not full then increment ID
			if (BuffMsgFind(&ReadWriteIO, &BufferPendingMsg, 0) != -1)
			{			
				packID++; 
				if (packID > 9999) 
					packID = 1; // avoid integer overflow.		 
			}


			// Message parameters.
			strcpy(msgUCS.prefix, MSG_PREFIX);	
			msgUCS.msgID = MSG_REQ_VALUE_TO_SERVER;
			msgUCS.packID = packID;
			msgUCS.checksum = 0;
			msgUCS.value = 0.0f;		
		
			// Build message string.
			BuildMessageString(&msgUCS, messageBuffer);	 
			
			// Request data to server through communication socket.
			if (write(sock1, messageBuffer, strlen(messageBuffer)) 
					!= strlen(messageBuffer))
			{
				//if (ShowGeneralMsg)
					RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
						"|##ERROR##| Request #", &(msgUCS.packID), DATA_INT);
			}
			else
			{
				//if (ShowGeneralMsg)
				RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
						"|CLIENT >>| Request #", &(msgUCS.packID), DATA_INT);
				
			}		
			
			// Insert message into pending message buffer, while client 
			// waits for value from server.
			if (BuffMsgInsert(&ReadWriteIO, &BufferPendingMsg, 
								&msgUCS) == false)
			{
				// Message buffer is full. Message will not be stored.
				//if (ShowGeneralMsg)
				 	RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
					"|##ERROR##| Buffer full #", &(msgUCS.packID), DATA_INT);
			}
			else
			{
				if (ShowGeneralMsg)
					RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
						"|  CLIENT | Inserted in the pending Buffer #", &(msgUCS.packID), DATA_INT);
			}

				


		}



		      
        // Calculate start time of next execution period.
        thTime.tv_sec += thPeriodSec;
		thTime.tv_nsec += thPeriodNsec;
		if(thTime.tv_nsec >= NSEC_PER_SEC) 
		{
			// Correct variables every 1 second.
			thTime.tv_nsec -= NSEC_PER_SEC;
			thTime.tv_sec++;
		}
		
		ReadGlobalVar(&ReadWriteIO, &QuitProgram, &quitP, DATA_BOOL);
    } 
        	
	pthread_exit(NULL);
}

// ------------------------------------

void ReceiveData()
{
	// Receive messages from server.
	bool ShowGeneralMsg =true;
	int lastId =0;
	bool quitP = false;
	bool sendAckNack;
	char messageBuffer[BUF_SIZE];
	int sock1, sock2;
	unsigned int linePrint = 0;
	float varRead;
	ssize_t sizeRead;
	struct sockaddr_in server; 
	struct sockaddr_storage client;
	socklen_t client_addr_len = sizeof(struct sockaddr_storage);
	struct MessageUCS msgUCS, msgPendingUCS;
	
	// Start communication socket.		
	sock1 = GetCommunicationSocket(NULL, COMM_UDP_PORT_TO_CLIENT, COMM_RECEIVER);
	sock2 = GetCommunicationSocket(COMM_SERVER_IP, COMM_UDP_PORT_TO_SERVER, 
									COMM_TRANSMITTER);	
	
	while(!quitP)
	{		
		
		// get value of ShowGeneralMsg
			ReadGlobalVar(&ReadWriteIO, &ShowGeneralMsgOn, &ShowGeneralMsg, DATA_BOOL);

		// Check for server replies.	
		strcpy(messageBuffer, "");	
		sizeRead = recvfrom(sock1, messageBuffer, BUF_SIZE, 0,
                (struct sockaddr *) &client, &client_addr_len);
        if (sizeRead != -1)
        {
			// Reply received.
			
			// Extract message information.
			ReadMessageString(messageBuffer, &msgUCS);
						
			switch(msgUCS.msgID)
			{
				case MSG_SEND_VALUE_TO_CLIENT:
					// Server is sending value to client.			
									
					if (ShowGeneralMsg)
						RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
						"|>> CLIENT| Reply #", &(msgUCS.packID), DATA_INT);				
					
					// Check message validity.
					if (CheckMsgValidity(&msgUCS) == true)
					{
						// Message is valid. Send ACK to server.
						msgUCS.msgID = MSG_ACK;						

					}
					else
					{
						// Message is not valid. Send NACK to server.
						msgUCS.msgID = MSG_NACK;
						//if (ShowGeneralMsg)
							RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
							"|##ERROR##| Checksum #", &(msgUCS.packID), DATA_INT);															
					}	
					
					msgUCS.checksum = CalculateChecksum(&msgUCS, 
										&ReadWriteIO, &ChecksumErrorOn);
					
				


                    //se msg eh repetida e ja saiu da fila
					if ((lastId >= msgUCS.packID)& (BuffMsgRemove(&ReadWriteIO, &BufferPendingMsg, 
									&msgUCS) == false))
					{

						if (ShowGeneralMsg)
							RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
								"|>>CLIENT| RECEBIDO PACOTE REPETIDO #", &(msgUCS.packID), DATA_INT);


						if (sendAckNack == true)
					{
						// Send ACK/NACK to server.
						BuildMessageString(&msgUCS, messageBuffer);	
						
						if (msgUCS.msgID == MSG_ACK)
						{
							if (ShowGeneralMsg)
								RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
								"|CLIENT >>| ACK #", &(msgUCS.packID), DATA_INT);
						}
						else if (msgUCS.msgID == MSG_NACK)
						{
							if (ShowGeneralMsg)
								RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
								"|CLIENT >>| NACK #", &(msgUCS.packID), DATA_INT);
						}
													
						if (write(sock2, messageBuffer, 
								strlen(messageBuffer)) != strlen(messageBuffer))		
						{
							// Error sending response.
							//if (ShowGeneralMsg)
								RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
								"|##ERROR##| ACK/NACK #", &(msgUCS.packID), DATA_INT);
						}  
					}
					}


					// se mensagem chegou na ordem e valida
					if ((lastId == msgUCS.packID - 1)& (msgUCS.msgID == MSG_ACK))
					{

					if (BuffMsgRemove(&ReadWriteIO, &BufferPendingMsg, 
									&msgUCS) == true);
					{
						lastId = msgUCS.packID;
						
						// Print received value on screen.
						RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
							"| SENSOR  | Temperature: ", &(msgUCS.value), DATA_FLOAT);
						RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
							"| SENSOR  | PackID: ", &(msgUCS.packID), DATA_INT);

					}

					if (ShowGeneralMsg)
						RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
						"|  CLIENT | Removed from the pending Buffer #", &lastId, DATA_INT);	
	
					ReadGlobalVar(&ReadWriteIO, &SendACKNACKOn, 
										&sendAckNack, DATA_BOOL);
					

					if (sendAckNack == true)
					{
						// Send ACK/NACK to server.
						BuildMessageString(&msgUCS, messageBuffer);	
						
						if (msgUCS.msgID == MSG_ACK)
						{
							if (ShowGeneralMsg)
								RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
								"|CLIENT >>| ACK #", &(msgUCS.packID), DATA_INT);
						}
						else if (msgUCS.msgID == MSG_NACK)
						{
							if (ShowGeneralMsg)
								RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
								"|CLIENT >>| NACK #", &(msgUCS.packID), DATA_INT);
						}
													
						if (write(sock2, messageBuffer, 
								strlen(messageBuffer)) != strlen(messageBuffer))		
						{
							// Error sending response.
							//if (ShowGeneralMsg)
								RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
								"|##ERROR##| ACK/NACK #", &(msgUCS.packID), DATA_INT);
						}  
					}
					
					// Check if there are older messages not replied.
					if (BuffMsgOldest(&ReadWriteIO, 
						&BufferPendingMsg, &msgPendingUCS) == true)	
					{
						// Seize that communication with server was made
						// to resend older message that is still not
						// replied.
						// At this moment only one message will be resent,
						// the one with lowest package ID.
						
						BuildMessageString(&msgPendingUCS, messageBuffer);
						if (ShowGeneralMsg)
							RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
								"|CLIENT >>| New Request #", 
								&(msgPendingUCS.packID), DATA_INT);								
						
						// Request data to server.
						if (write(sock2, messageBuffer, strlen(messageBuffer)) 
								!= strlen(messageBuffer))	
						{
							// Error sending request.
							if (ShowGeneralMsg)
								RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
								"|##ERROR##| New Request #", 
								&(msgPendingUCS.packID), DATA_INT);
						}  						
					}	


					}									
					break;					
				
				default:
					RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
						"|>> CLIENT| Unknown message received: ", messageBuffer, DATA_STRING);
					break;
			}			
		}
		
		ReadGlobalVar(&ReadWriteIO, &QuitProgram, &quitP, DATA_BOOL);
	} 
	
	// Terminate this thread.
	pthread_exit(NULL);
}

// ------------------------------------
