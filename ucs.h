
/*  
	Developed by Bruno, Gabriel and Ramiro in 2016 for TISA.		
    
    Client/server application communicating through UDP sockets on Unix.    
    Developed on Linux Fedora.     
     
    Program functional description:
    
		- Server:
			- Acquires data from sensor continuously.
			- Waits for client requests.
			
		- Clients:
			- Request data from server continuously.
			
		- The message exchange between server and clients has the 
		following sequence:
			1 - Client sends request to server;
			2 - Server replies with sensor value to client;
			3 - Client acknowledges reception to server.
			- If client does not receive reply from server, it repeats
		the request to the first pending message in the buffer, after 
		sending	the current ACK.
			- If server does not receive ACK from client, it resends
		the value of the first pending message in the buffer, after 
		replying to the current request.  
	
		
	Compiled through command './compileUCS'.
	To run: './server' in one computer terminal, 
			'./client' in another computer terminal.
	Client and server IPs must be defined.  
*/

/*
	Common functions and definitions for server and clients.
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/types.h>

#define BUF_SIZE		1000
#define NSEC_PER_SEC    1000000000 // nano seconds per second.
#define DATA_NULL	0
#define DATA_BOOL	1
#define DATA_INT	2
#define DATA_FLOAT	3
#define DATA_STRING 4
#define COMM_RECEIVER		1
#define COMM_TRANSMITTER	2

// Communication parameters.
#define COMM_SERVER_IP			"127.0.0.1"	// Server IP.
#define COMM_CLIENT_IP			"127.0.0.1" // Client IP.
#define COMM_UDP_PORT_TO_SERVER	"4546"    	// UDP port for messages to server.
#define COMM_UDP_PORT_TO_CLIENT	"4547"    	// UDP port for messages to clients.

// List of messages. 
#define MSG_PREFIX  				"ucs"
#define MSG_ACK						01
#define MSG_NACK					02
#define MSG_SEND_VALUE_TO_CLIENT	03
#define MSG_REQ_VALUE_TO_SERVER     04

#define MAX_NB_MSG	5 // Maximum number of pending messages on buffer.

// Structure of client/server messages.
struct MessageUCS
{	
	char prefix[BUF_SIZE]; 	// Message prefix.
	unsigned int msgID;		// Message identifier.
	unsigned int packID;	// Package identifier.
	unsigned int checksum;	// Checksum.
	float value; 			// Value sent.
};

// Buffer of still pending messages.
struct BufferMessageUCS
{	
	unsigned int nbMsg;					// Number of messages on buffer.
	struct MessageUCS Msg[MAX_NB_MSG];	// Buffer of messages.
};

// Create communication socket.
int GetCommunicationSocket(char *pHost, char *pPort, unsigned int type);
// Funtions to safely manipulate global variables among different threads.
void ReadGlobalVar(pthread_mutex_t *mutexID, void *pGlobalVar, 
						void *pLocalVar, unsigned int dataType);
void WriteGlobalVar(pthread_mutex_t *mutexID, void *pGlobalVar, 
						void *pNewValue, unsigned int dataType);
// Functions to manipulate buffer of messages.
void BuffMsgInit(pthread_mutex_t *mutexID, struct BufferMessageUCS *pBuffMsg);
bool BuffMsgInsert(pthread_mutex_t *mutexID, 
	struct BufferMessageUCS *pBuffMsg, struct MessageUCS *pMsgUCS);
int BuffMsgFind(pthread_mutex_t *mutexID, 
	struct BufferMessageUCS *pBuffMsg, unsigned int packID);
bool BuffMsgRemove(pthread_mutex_t *mutexID, 
	struct BufferMessageUCS *pBuffMsg, struct MessageUCS *pMsgUCS);	
bool BuffMsgNext(pthread_mutex_t *mutexID, 
	struct BufferMessageUCS *pBuffMsg, struct MessageUCS *pMsgUCS, int last);		
bool BuffMsgOldest(pthread_mutex_t *mutexID, 
	struct BufferMessageUCS *pBuffMsg, struct MessageUCS *pMsgUCS);		
// Print data on screen.	
void RefreshScreen(pthread_mutex_t *mutexID, bool *pPrintOnScreen, 
					unsigned int *pLinePrint, char *pText, 
					void *pValue, unsigned int dataType);
// Functions to manipulate messages.					
void BuildMessageString(struct MessageUCS *pMsgUCS, char *pMessageBuffer);
void ReadMessageString(char *pMessageBuffer, struct MessageUCS *pMsgUCS);
unsigned int CalculateChecksum(struct MessageUCS *pMsgUCS, 
					pthread_mutex_t *mutexID, bool *pChecksumErrorOn);
bool CheckMsgValidity(struct MessageUCS *pMsgUCS);

// ------------------------------------

int GetCommunicationSocket(char *pHost, char *pPort, unsigned int type)
{
	// Open communication socket.
	
	int sock;
	int resGetInfo;
	struct addrinfo addrSim;
	struct addrinfo *pResultAddr, *piResult;	
	
	if(type == COMM_RECEIVER)
	{
		printf("Establishing receiver socket on port %s ... ", pPort);		

		memset(&addrSim, 0, sizeof(struct addrinfo));
		addrSim.ai_family = AF_UNSPEC; 		// Allow IPv4 or IPv6.
		addrSim.ai_socktype = SOCK_DGRAM; 	// Datagram socket.
		addrSim.ai_flags = AI_PASSIVE;		// For wildcard IP address.
		addrSim.ai_protocol = 0;          	// Any protocol.
		addrSim.ai_canonname = NULL;
		addrSim.ai_addr = NULL;
		addrSim.ai_next = NULL;
	}
	else if(type == COMM_TRANSMITTER)
	{
		printf("Establishing transmitter socket with IP %s port %s ... ", 
		pHost, pPort);		

		memset(&addrSim, 0, sizeof(struct addrinfo));
		addrSim.ai_family = AF_UNSPEC; 		// Allow IPv4 or IPv6.
		addrSim.ai_socktype = SOCK_DGRAM; 	// Datagram socket.
		addrSim.ai_flags = 0;
		addrSim.ai_protocol = 0;          	// Any protocol.
	}
   
    // Get list of addresses matching host/port.
    resGetInfo = getaddrinfo(pHost, pPort, &addrSim, &pResultAddr);
    if (resGetInfo != 0) 
    {
		// Error.
        printf("\n!ERROR: Did not get address info.\n\n");
        exit(EXIT_FAILURE);
    }
    
    // Try each address until a successful connection is established.
	for (piResult = pResultAddr; piResult != NULL; piResult = piResult->ai_next) 
	{
		sock = socket(piResult->ai_family, piResult->ai_socktype, piResult->ai_protocol);
        if (sock == -1) 
            continue; // Go to next iteration.

		if(type == COMM_RECEIVER)
		{
			if (bind(sock, piResult->ai_addr, piResult->ai_addrlen) == 0)
				break;	// Connection successful.
		}
		else if(type == COMM_TRANSMITTER)
		{
			if (connect(sock, piResult->ai_addr, piResult->ai_addrlen) != -1)
				break;	// Connection successful.
		}

		// Close socket and go to next iteration.
        close(sock); 
    }    

    if (piResult == NULL) 
    {               
		// No address succeeded.
        printf("\n!ERROR: Could not connect.\n\n");
        exit(EXIT_FAILURE);
    }

    freeaddrinfo(pResultAddr); // No longer needed.
	printf("OK!\n");
	return sock;
}

// ------------------------------------

void ReadGlobalVar(pthread_mutex_t *mutexID, void *pGlobalVar, 
						void *pLocalVar, unsigned int dataType)
{
	// "Atomic" function to access global variables from different threads. 
	pthread_mutex_lock(mutexID);
		// Read global variable value.
		switch(dataType)
		{
			case(DATA_BOOL):
				*(bool*)pLocalVar = *(bool*)pGlobalVar; 
				break;
				
			case(DATA_INT):
				*(int*)pLocalVar = *(int*)pGlobalVar; 
				break;
				
			case(DATA_FLOAT):
				*(float*)pLocalVar = *(float*)pGlobalVar;
				break;
		}		 
    pthread_mutex_unlock(mutexID); 
}

// ------------------------------------

void WriteGlobalVar(pthread_mutex_t *mutexID, void *pGlobalVar, 
						void *pNewValue, unsigned int dataType)
{
	// "Atomic" function to access global variables from different threads. 
	pthread_mutex_lock(mutexID);
		// Write new value to global variable.
		switch(dataType)
		{
			case(DATA_BOOL):
				*(bool*)pGlobalVar = *(bool*)pNewValue;
				break;
				
			case(DATA_INT):
				*(int*)pGlobalVar = *(int*)pNewValue;
				break;
				
			case(DATA_FLOAT):
				*(float*)pGlobalVar = *(float*)pNewValue; 
				break;
		}
    pthread_mutex_unlock(mutexID);
}

// ------------------------------------

void BuffMsgInit(pthread_mutex_t *mutexID, struct BufferMessageUCS *pBuffMsg)
{
	// Fill whole buffer with initial values.
	
	int i;
	
	// "Atomic" function to access global variables from different threads. 
	pthread_mutex_lock(mutexID);
	
		pBuffMsg->nbMsg = 0;
		for (i=0; i < MAX_NB_MSG; i++)
		{
			strcpy(pBuffMsg->Msg[i].prefix, "");
			pBuffMsg->Msg[i].msgID = 0;
			pBuffMsg->Msg[i].packID = 0; // packID = 0 means free space.
			pBuffMsg->Msg[i].checksum = 0;
			pBuffMsg->Msg[i].value = 0.0f;		
		}
				
    pthread_mutex_unlock(mutexID);
}

// ------------------------------------

bool BuffMsgInsert(pthread_mutex_t *mutexID, 
	struct BufferMessageUCS *pBuffMsg, struct MessageUCS *pMsgUCS)
{
	// Insert message into message buffer.
	
	int iFree;
	unsigned int nbMsg;
	
	
	pthread_mutex_lock(mutexID);
		nbMsg = pBuffMsg->nbMsg;
	pthread_mutex_unlock(mutexID);	
	if (nbMsg == MAX_NB_MSG)
	{
		// Buffer is full. Message will not be inserted.		
		return false;
	}
	
	// Search for empty space on message buffer.
	iFree = BuffMsgFind(mutexID, pBuffMsg, 0);
	if (iFree == -1)
	{
		// Empty space not found. Message will not be inserted.
		return false;
	}
	
	// Copy message info to message buffer.
	pthread_mutex_lock(mutexID);		
	
		strcpy(pBuffMsg->Msg[iFree].prefix, pMsgUCS->prefix);
		pBuffMsg->Msg[iFree].msgID = pMsgUCS->msgID;
		pBuffMsg->Msg[iFree].packID = pMsgUCS->packID; 
		pBuffMsg->Msg[iFree].checksum = pMsgUCS->checksum;
		pBuffMsg->Msg[iFree].value = pMsgUCS->value;		
		
		pBuffMsg->nbMsg = pBuffMsg->nbMsg + 1;
				
    pthread_mutex_unlock(mutexID);				
   
    return true;
}

// ------------------------------------

bool BuffMsgRemove(pthread_mutex_t *mutexID, 
	struct BufferMessageUCS *pBuffMsg, struct MessageUCS *pMsgUCS)
{
	// Remove message from message buffer.
	
	int iMsg;
	unsigned int nbMsg;	
	
	// Search for message position on message buffer.
	iMsg = BuffMsgFind(mutexID, pBuffMsg, pMsgUCS->packID);
	if (iMsg == -1)
	{
		// Message not found.
		return false;
	}
	
	// Delete message info from message buffer.
	pthread_mutex_lock(mutexID);		
	
		strcpy(pBuffMsg->Msg[iMsg].prefix, "");
		pBuffMsg->Msg[iMsg].msgID = 0;
		pBuffMsg->Msg[iMsg].packID = 0;  // packID = 0 means free space
		pBuffMsg->Msg[iMsg].checksum = 0;
		pBuffMsg->Msg[iMsg].value = 0.0f;		
		
		pBuffMsg->nbMsg = pBuffMsg->nbMsg - 1;
		
    pthread_mutex_unlock(mutexID);				
   
    return true;
}

// ------------------------------------

int BuffMsgFind(pthread_mutex_t *mutexID, 
	struct BufferMessageUCS *pBuffMsg, unsigned int packID)
{
	// Find defined package within message buffer.
	
	int i;
	
	// "Atomic" function to access global variables from different threads. 
	pthread_mutex_lock(mutexID);
		for (i=0; i < MAX_NB_MSG; i++)
		{
			if (pBuffMsg->Msg[i].packID == packID)
			{
				// Return position of package within message buffer.
				pthread_mutex_unlock(mutexID);
				return i;
			}
		}
	pthread_mutex_unlock(mutexID);
	// Package not found.
    return -1;
}

// ------------------------------------

bool BuffMsgNext(pthread_mutex_t *mutexID, 
	struct BufferMessageUCS *pBuffMsg, struct MessageUCS *pMsgUCS, int last)	
{
	// Find next pending message within the buffer with id greater than last.
	
	int i,idMin;
	int packIDmin = 999999;
	unsigned int nbMsg;	
	
	pthread_mutex_lock(mutexID);
		nbMsg = pBuffMsg->nbMsg;
	pthread_mutex_unlock(mutexID);
	
	if (nbMsg == 0)
	{
		// Buffer is empty.		
		return false;
	}
	
	idMin =-1;
	// "Atomic" function to access global variables from different threads. 
	pthread_mutex_lock(mutexID);
		// Search for the pending message with the lowest package ID.
		for (i=0; i < MAX_NB_MSG; i++)
		{
			if ((pBuffMsg->Msg[i].packID != 0)&(pBuffMsg->Msg[i].packID > last))
			{
				// Message is pending.
				if (pBuffMsg->Msg[i].packID < packIDmin)
				{
					// So far it has the lowest package ID.
					packIDmin = pBuffMsg->Msg[i].packID;
					idMin = i;
				}
			}
		}
		

		// Copy info message.
		strcpy(pMsgUCS->prefix, pBuffMsg->Msg[idMin].prefix);
		pMsgUCS->msgID = pBuffMsg->Msg[idMin].msgID;
		pMsgUCS->packID = pBuffMsg->Msg[idMin].packID; 
		pMsgUCS->checksum = pBuffMsg->Msg[idMin].checksum;
		pMsgUCS->value = pBuffMsg->Msg[idMin].value;
		
	pthread_mutex_unlock(mutexID);

	if (idMin == -1)
		{
			return false;
		}
		else
		{
    		return true;
		}	
}
// ------------------------------------

bool BuffMsgOldest(pthread_mutex_t *mutexID, 
	struct BufferMessageUCS *pBuffMsg, struct MessageUCS *pMsgUCS)
{
	// Find next pending message within the buffer.
	
	int i;
	int packIDmin = 999999;
	unsigned int nbMsg, idMin;	
	
	pthread_mutex_lock(mutexID);
		nbMsg = pBuffMsg->nbMsg;
	pthread_mutex_unlock(mutexID);
	
	if (nbMsg == 0)
	{
		// Buffer is empty.		
		return false;
	}
	
	// "Atomic" function to access global variables from different threads. 
	pthread_mutex_lock(mutexID);
		// Search for the pending message with the lowest package ID.
		for (i=0; i < MAX_NB_MSG; i++)
		{
			if (pBuffMsg->Msg[i].packID != 0)
			{
				// Message is pending.
				if (pBuffMsg->Msg[i].packID < packIDmin)
				{
					// So far it has the lowest package ID.
					packIDmin = pBuffMsg->Msg[i].packID;
					idMin = i;
				}
			}
		}
		
		// Copy info message.
		strcpy(pMsgUCS->prefix, pBuffMsg->Msg[idMin].prefix);
		pMsgUCS->msgID = pBuffMsg->Msg[idMin].msgID;
		pMsgUCS->packID = pBuffMsg->Msg[idMin].packID; 
		pMsgUCS->checksum = pBuffMsg->Msg[idMin].checksum;
		pMsgUCS->value = pBuffMsg->Msg[idMin].value;
		
	pthread_mutex_unlock(mutexID);

    return true;
}

// ------------------------------------

void RefreshScreen(pthread_mutex_t *mutexID, bool *pPrintOnScreen, 
					unsigned int *pLinePrint, char *pText, 
					void *pValue, unsigned int dataType)
{
	// Display value on screen. 			
	
	bool printS;
	
	// Read global variable that indicates if printing is enabled.
	ReadGlobalVar(mutexID, (void*) pPrintOnScreen, &printS, DATA_BOOL);
	
	if (printS == true)
	{	
		// Printing on screen is enabled. Display information.	
		
		// Print introductory string.
		printf("%s", pText); 
		// Print value.	
		switch(dataType)
		{
			case(DATA_NULL):
				break;
				
			case(DATA_BOOL):
				printf("%1u", *(bool*)pValue); 
				break;
				
			case(DATA_INT):
				printf("%d", *(int*)pValue);
				break;
				
			case(DATA_FLOAT):
				printf("%6.2f", *(float*)pValue);
				break;
				
			case(DATA_STRING):
				printf("%s", (char*)pValue);
				break;	
		}
					
		if ((*pLinePrint % 10) == 0)
		{
			// Display information to access menu after some lines.
			printf(" | Press <ENTER> for menu.");
			*pLinePrint = 0;
		}
		*pLinePrint = *pLinePrint + 1;
		printf("\n");
	}
}

// ------------------------------------

void BuildMessageString(struct MessageUCS *pMsgUCS, char *pMessageBuffer)
{
	// Message format: 
	// <MSG_PREFIX(3)>-<MSG_ID(1)>-<PACK_ID(4)>-<CHECKSUM(8)>-<VALUE> 
	
	char temp1[BUF_SIZE];	
	
	strcpy(pMessageBuffer, "");
	
	// Build message string.
	strcpy(pMessageBuffer, pMsgUCS->prefix); 	// common for all messages.
	strcpy(temp1, "");
	sprintf(temp1, "-%02u", pMsgUCS->msgID);	// message identification.
	strcat(pMessageBuffer, temp1);
	strcpy(temp1, "");
	sprintf(temp1, "-%04u", pMsgUCS->packID);	// package identification.
	strcat(pMessageBuffer, temp1);
	strcpy(temp1, "");
	sprintf(temp1, "-%08d", pMsgUCS->checksum);	// checksum.
	strcat(pMessageBuffer, temp1);
	strcpy(temp1, "");
	sprintf(temp1, "-%06.2f", pMsgUCS->value);	// value to be sent.
	strcat(pMessageBuffer, temp1); 	
	// printf("Message: '%s'.", pMessageBuffer);
}

// ------------------------------------

void ReadMessageString(char *pMessageBuffer, struct MessageUCS *pMsgUCS)
{
	// Message format: 
	// <MSG_PREFIX(3)>-<MSG_ID(1)>-<PACK_ID(4)>-<CHECKSUM(8)>-<VALUE> 
	
	// Extract values from message string.
	sscanf(pMessageBuffer, "%3s-%2u-%4u-%8d-%f", pMsgUCS->prefix, &(pMsgUCS->msgID),
			&(pMsgUCS->packID), &(pMsgUCS->checksum), &(pMsgUCS->value));
}

// ------------------------------------

unsigned int CalculateChecksum(struct MessageUCS *pMsgUCS, 
					pthread_mutex_t *mutexID, bool *pChecksumErrorOn)
{
	// Calculate checksum for future error detection.	
	
	bool checksumError;
	unsigned int checksum;

	// Calculate checksum.
	// Algorithm consists of simple XOR among some message parameters.
	checksum = pMsgUCS->msgID ^ pMsgUCS->packID 
				^ (unsigned int) pMsgUCS->value;
	
	if (pChecksumErrorOn != NULL)
	{			
		ReadGlobalVar(mutexID, (void*) pChecksumErrorOn, 
						&checksumError, DATA_BOOL);			
		if(checksumError == true)
		{
			// Checksum error will be artificially inserted into message,
			// for testing purposes.
			checksum = !checksum;
		}
	}				
	
	return checksum;	
}

// ------------------------------------

bool CheckMsgValidity(struct MessageUCS *pMsgUCS)
{
	// Check message validity.
	
	if (pMsgUCS->checksum == CalculateChecksum(pMsgUCS, NULL, NULL))
		return true; 	// Message is valid.
	else
		return false;	// Message is not valid.
}

// ------------------------------------
