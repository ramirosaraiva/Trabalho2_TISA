
/*  
    Compiled through command 'gcc -pthread -o server unixServer.c -lrt'
    or './compileUCS'.
	To run: ./server
*/

#include "ucs.h" 	

// Threads execution period (in seconds).
#define DATA_READ_TS	3.0f	// data read: 3.0 s.

// Global variables.
bool PrintOnScreen;
bool QuitProgram;
bool ReplyClientOn;
bool ChecksumErrorOn;
bool ShowGeneralMsgOn;

float TempMeas = 0.0f; 	// Temperature measurement.
struct BufferMessageUCS BufferPendingMsg; // Buffer of pending messages.
pthread_mutex_t ReadWriteIO = PTHREAD_MUTEX_INITIALIZER;

void ReadSensorData();
void ReceiveClientMessage();

// ------------------------------------

int main (int argc, char *argv[]) 
{
	// Main function.		
	
	char comando;
	bool quitP, printS, replyClient, checksumError,ShowGeneralMsg;
	pthread_t thReadSensor, thReceiveClientMessage;	
			
	printf("\n--- SERVER ---\n");					
	
	// Initiate variables.
	BuffMsgInit(&ReadWriteIO, &BufferPendingMsg);
	quitP = false;
	WriteGlobalVar(&ReadWriteIO, &QuitProgram, &quitP, DATA_BOOL);	
	printS = true;
	WriteGlobalVar(&ReadWriteIO, &PrintOnScreen, &printS, DATA_BOOL);
	replyClient = true;
	WriteGlobalVar(&ReadWriteIO, &ReplyClientOn, &replyClient, DATA_BOOL);	
	checksumError = false;
	WriteGlobalVar(&ReadWriteIO, &ChecksumErrorOn, &checksumError, DATA_BOOL);
	ShowGeneralMsg = false;
	WriteGlobalVar(&ReadWriteIO, &ShowGeneralMsgOn, &ShowGeneralMsg, DATA_BOOL);

	// Create threads of execution.	
	pthread_create(&thReadSensor, NULL, 
			(void*) ReadSensorData, NULL); 			// Read sensor data.	
	pthread_create(&thReceiveClientMessage, NULL, 
			(void*) ReceiveClientMessage, NULL); 	// Wait for client requests.		
			
	while(!quitP)
	{
		// Wait for keyboard input command.		
		scanf("%c", &comando);		
		
		switch(comando)
		{	
			case 10: // <Enter> key, load Menu.
				// Stop printing process data on screen.
				printS = false;
				WriteGlobalVar(&ReadWriteIO, &PrintOnScreen, 
								&printS, DATA_BOOL);
				
				printf("Type '1'+<Enter> to start/stop sending reply to clients.\n");
				printf("Type '2'+<Enter> to start/stop sending wrong checksum.\n");
				printf("Type '3'+<Enter> to start/stop showing general messages.\n");
				printf("Type 'q'+<Enter> to quit program.\n");
				printf("Press <Enter> to leave menu.\n");	
				
				while(!printS)
				{
					// Wait for keyboard input command.		
					scanf("%c", &comando);
					
					switch(comando)
					{						
						case '1':
							// Start/stop sending reply to clients.
							ReadGlobalVar(&ReadWriteIO, &ReplyClientOn, 
											&replyClient, DATA_BOOL);
							if (replyClient == true)
							{
								// Turn off replies.
								replyClient = false;
								printf("Replies turned off.\n");								
							}
							else
							{
								// Turn on replies.
								replyClient = true;								
								printf("Replies turned on.\n");	
							}
							WriteGlobalVar(&ReadWriteIO, &ReplyClientOn, 
												&replyClient, DATA_BOOL);
							break;
							
						case '2':
							// Start/stop sending wrong checksum to clients.
							ReadGlobalVar(&ReadWriteIO, &ChecksumErrorOn, 
											&checksumError, DATA_BOOL);
							if (checksumError == true)
							{
								// Turn off checksum errors.
								checksumError = false;
								printf("Checksum errors turned off.\n");								
							}
							else
							{
								// Turn on checksum errors.
								checksumError = true;								
								printf("Checksum errors turned on.\n");	
							}
							WriteGlobalVar(&ReadWriteIO, &ChecksumErrorOn, 
												&checksumError, DATA_BOOL);
							break;


						case '3':
							// Start/stop sending wrong checksum to server.
							ReadGlobalVar(&ReadWriteIO, &ShowGeneralMsgOn, 
											&ShowGeneralMsg, DATA_BOOL);
							if (ShowGeneralMsg == true)
							{
								// Turn off general messages.
								ShowGeneralMsg = false;
								printf("General Messages turned off.\n");								
							}
							else
							{
								// Turn on general messages.
								ShowGeneralMsg = true;								
								printf("General Messages turned on.\n");	
							}
							WriteGlobalVar(&ReadWriteIO, &ShowGeneralMsgOn, 
												&ShowGeneralMsg, DATA_BOOL);
							break;	

						case 'q':
							// Quit program.
							printS = true;
							WriteGlobalVar(&ReadWriteIO, &PrintOnScreen, 
											&printS, DATA_BOOL);
							// Terminate control loops.
							quitP = true;
							WriteGlobalVar(&ReadWriteIO, &QuitProgram, 
											&quitP, DATA_BOOL);													
							break;
							
						case 10: // <Enter> key, close Menu.
							// Restart printing process data on screen.
							printS = true;
							WriteGlobalVar(&ReadWriteIO, &PrintOnScreen, 
											&printS, DATA_BOOL);
							break;	
							
						default:
							printf("Unknown command.\n");
							break;
					}
						
				}				
				break;			
				
			default:
				break;
		}	
		
		ReadGlobalVar(&ReadWriteIO, &QuitProgram, &quitP, DATA_BOOL);
		
	}

	// Does not finish main function until some threads have finished.
	pthread_join(thReadSensor, NULL);
	// pthread_join(thReceiveClientMessage, NULL);

	printf("BYE!\n\n");
	return 0; // end of program.
}	

// ------------------------------------

void ReadSensorData()
{
	// Read measurement value. 	

	bool quitP = false;
	char messageBuffer[BUF_SIZE];	
	unsigned int linePrint = 0;	
	long int thPeriodSec, thPeriodNsec;
	float varRead;
	struct timespec thTime;	
	FILE *fileData = NULL;
	bool ShowGeneralMsg =0;
	
	// Convert thread execution period.
	thPeriodSec = (long int) DATA_READ_TS;
	thPeriodNsec = (long int) ((DATA_READ_TS-thPeriodSec)*NSEC_PER_SEC);
	
	// Open data file.
	fileData = fopen("temp1.txt", "r+");
	if (fileData == NULL) 
	{
		printf("\n!ERROR: Error opening file.\n\n");
		exit(EXIT_FAILURE);
	}	
		
	// Get current time.
    clock_gettime(CLOCK_MONOTONIC ,&thTime);
    // Start after one second. 
    thTime.tv_sec++;    
    	
	while (!quitP)
	{	// get value of ShowGeneralMsg
		ReadGlobalVar(&ReadWriteIO, &ShowGeneralMsgOn, &ShowGeneralMsg, DATA_BOOL);	

		// Wait until next execution period starts.
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &thTime, NULL);

		// Start loop execution.			
	
		// Go to the beggining of the file.
		rewind (fileData);			  
		// Read data from file.
		fscanf (fileData, "%f", &varRead);				
		WriteGlobalVar(&ReadWriteIO, &TempMeas, &varRead, DATA_FLOAT);
	
		// Print data on screen.
		//if (ShowGeneralMsg)
		//	RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
		//		"| SENSOR  | Temperature: ", &varRead, DATA_FLOAT);		        
		      
        // Calculate start time of next execution period.
        thTime.tv_sec += thPeriodSec;
		thTime.tv_nsec += thPeriodNsec;
		if(thTime.tv_nsec >= NSEC_PER_SEC) 
		{
			// Correct variables every 1 second.
			thTime.tv_nsec -= NSEC_PER_SEC;
			thTime.tv_sec++;
		}
		
		ReadGlobalVar(&ReadWriteIO, &QuitProgram, &quitP, DATA_BOOL);
    } 
        	
	// Terminate this thread.
	fclose(fileData);
	pthread_exit(NULL);
}

// ------------------------------------

void ReceiveClientMessage()
{
	// Receive requests and ACK/NACK from clients, send replies to them.
	int lastID=0;
	int lastIdSearched =0;
	bool quitP = false;
	bool replyClient;
	char messageBuffer[BUF_SIZE];
	char host[NI_MAXHOST], service[NI_MAXSERV];
	int sock1, sock2;
	int sHost;
	unsigned int linePrint = 0;
	float varRead;
	ssize_t sizeRead;
	struct sockaddr_in server; 
	struct sockaddr_storage client;
	socklen_t client_addr_len = sizeof(struct sockaddr_storage);
	struct MessageUCS msgUCS, msgPendingUCS;
	bool ShowGeneralMsg=0;
	
	// Start communication socket.		
	sock1 = GetCommunicationSocket(NULL, COMM_UDP_PORT_TO_SERVER, 
									COMM_RECEIVER);
	sock2 = GetCommunicationSocket(COMM_CLIENT_IP, 
									COMM_UDP_PORT_TO_CLIENT, COMM_TRANSMITTER);	
	
	while(!quitP)
	{	
		// get value of ShowGeneralMsg
		ReadGlobalVar(&ReadWriteIO, &ShowGeneralMsgOn, &ShowGeneralMsg, DATA_BOOL);	
		// Check for client requests.	
		strcpy(messageBuffer, "");		
		sizeRead = recvfrom(sock1, messageBuffer, BUF_SIZE, 0,
                (struct sockaddr *) &client, &client_addr_len);
        if (sizeRead != -1)
        {
			// Request received.
			
			// Extract message information.
			ReadMessageString(messageBuffer, &msgUCS);
						
			switch(msgUCS.msgID)
			{
				case MSG_REQ_VALUE_TO_SERVER:
					// Client requests value from server.
					
					// Check if replies are enabled.
					ReadGlobalVar(&ReadWriteIO, &ReplyClientOn, 
											&replyClient, DATA_BOOL);
					if (replyClient == true)
					{	
						// Discover which client sent the request.
						sHost = getnameinfo((struct sockaddr *) &client,
							client_addr_len, host, NI_MAXHOST,
							service, NI_MAXSERV, NI_NUMERICSERV);
						if (sHost != 0) { } // Host not identified.		
										
						//if (ShowGeneralMsg)
							RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
							"|>> SERVER| Request #", &(msgUCS.packID), DATA_INT);
						
						// Read measurement value from sensor.	
						ReadGlobalVar(&ReadWriteIO, &TempMeas, &varRead, DATA_FLOAT);										
						
						// Build message string.
						msgUCS.msgID = MSG_SEND_VALUE_TO_CLIENT;
						msgUCS.value = varRead;
						msgUCS.checksum = CalculateChecksum(&msgUCS, 
										&ReadWriteIO, &ChecksumErrorOn);	
						BuildMessageString(&msgUCS, messageBuffer);								
						
						// Send data to client.
						if (write(sock2, messageBuffer, strlen(messageBuffer)) 
								!= strlen(messageBuffer))	
						{
							// Error sending response.
							//if (ShowGeneralMsg)
								RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
								"|##ERROR##| Reply #", &(msgUCS.packID), DATA_INT);
						}
						else
						{
							//if (ShowGeneralMsg)
								RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
								"|SERVER >>| Reply #", &(msgUCS.packID), DATA_INT);
						}					
						
						// Check if there are older messages not acknowledged.
						//if (BuffMsgOldest(&ReadWriteIO, 
						//	&BufferPendingMsg, &msgPendingUCS) == true)	
						lastIdSearched=0;

						while(BuffMsgNext(&ReadWriteIO, 
								&BufferPendingMsg, &msgPendingUCS,lastIdSearched) == true)	
						{

							lastIdSearched = msgPendingUCS.packID;

							// Seize that communication with client was made
							// to resend older message that is still not
							// acknowledged.
							// At this moment only one message will be resent,
							// the one with lowest package ID.
							
							msgPendingUCS.msgID = MSG_SEND_VALUE_TO_CLIENT;
							msgPendingUCS.checksum = CalculateChecksum(
								&msgPendingUCS, &ReadWriteIO, &ChecksumErrorOn);
							BuildMessageString(&msgPendingUCS, messageBuffer);
//							if (ShowGeneralMsg)
//								RefreshScreen(&ReadWriteIO, &PrintOnScreen, 
//									&linePrint, "|SERVER >>| New Reply #", 
//									&(msgPendingUCS.packID), DATA_INT);								
							
							// Send data to client.
							if (write(sock2, messageBuffer, strlen(messageBuffer)) 
									!= strlen(messageBuffer))	
							{
								// Error sending response.
								//if (ShowGeneralMsg)
									RefreshScreen(&ReadWriteIO, &PrintOnScreen, 
									&linePrint, "|##ERROR##| New Reply #", 
									&(msgPendingUCS.packID), DATA_INT);
							}
							else
							{
						    	//if (ShowGeneralMsg)
						    		RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
									"|SERVER >>| Reply Again #", &(msgPendingUCS.packID), DATA_INT);
							}	 

						}
						

						//If this message is not alreadry in the buffer
						if (BuffMsgFind(&ReadWriteIO, &BufferPendingMsg, msgUCS.packID) == -1)
 						{
 							// then insert original message into pending message buffer, 
							// while server waits for an ACK from client.

							if (BuffMsgInsert(&ReadWriteIO, &BufferPendingMsg, 
											&msgUCS) == false)
							{
								// Message buffer is full. Message will not
								// be stored.
								//if (ShowGeneralMsg)
									RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
									"|##ERROR##| Buffer full #", &(msgUCS.packID), DATA_INT);
							}
							else
							{
								if (ShowGeneralMsg)
									RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
									"|  SERVER | Inserted in Pending Buffer #", &(msgUCS.packID), DATA_INT);
							}
						}
					}
						
					break;	
					
				case MSG_ACK:
					// Message acknowledgment from client.
					if (ShowGeneralMsg)
						RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
							"|>> SERVER| ACK #", &(msgUCS.packID), DATA_INT);
					if (CheckMsgValidity(&msgUCS) == true)
					{
						
						//If this message is alreadry in the buffer
						if (BuffMsgFind(&ReadWriteIO, &BufferPendingMsg, msgUCS.packID) != -1)
 						{

						// Remove message from pending messages buffer.
						if (BuffMsgRemove(&ReadWriteIO, &BufferPendingMsg, &msgUCS) == true)
						{
							if (ShowGeneralMsg)
								RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
									"| SERVER  | Removed from Pending Buffer #", &(msgUCS.packID), DATA_INT);
						}
						else
						{
							if (ShowGeneralMsg)
									RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
									"|  SERVER | Message not in buffer!!!!! #", &(msgUCS.packID), DATA_INT);

						}
						}
					}	
					else
					{
						// ACK message not valid. Message will be 
						// retransmitted.
							RefreshScreen(&ReadWriteIO, &PrintOnScreen, 
							&linePrint, "|##ERROR##| Checksum #", 
							&(msgUCS.packID), DATA_INT);	
					}
						
					break;
					
				case MSG_NACK:
					// Message not aknowledged by client. Message 
					// will be retransmitted.
					if (ShowGeneralMsg)
						RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
							"|>> SERVER| NACK #", &(msgUCS.packID), DATA_INT);								
					break;
				
				default:
					RefreshScreen(&ReadWriteIO, &PrintOnScreen, &linePrint, 
						"|>> SERVER| Unknown message received: ", messageBuffer, DATA_STRING);
					break;
			}			
		}
		
		ReadGlobalVar(&ReadWriteIO, &QuitProgram, &quitP, DATA_BOOL);
	} 
	
	// Terminate this thread.
	pthread_exit(NULL);
}

// ------------------------------------



